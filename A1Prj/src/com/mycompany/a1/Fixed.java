package com.mycompany.a1;


public abstract class Fixed extends GameObject
{ 

  public Fixed(int r, int g, int b, int s, double x, double y) {
	 super.setColor(r, g, b);
	 super.setSize(s);
	 super.setLocation(x,y);
	  
  }
  
  // another constructor to call 
  public Fixed() {
  }
  
  
  @Override
  public void setLocation(double a, double b)
   {
	 // empty body.  to prevent fixed location from changing location once created
    }
  
  @Override
   public void setSize(int a) {} // cannot change size
  }
  
 