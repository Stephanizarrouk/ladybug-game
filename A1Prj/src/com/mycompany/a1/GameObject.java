package com.mycompany.a1;

import java.util.Random;

import com.codename1.charts.util.ColorUtil;


public abstract class GameObject {
// private fields
	private int size;
	private int color; 
	// location in x, y
	private double x;
	private double y;
	Random rand = new Random ();
	private float Rx = rand.nextFloat() * 1024;
	private float Ry = rand.nextFloat() * 768;
	private int Rsize = rand.nextInt(40) + 10; // random size from 10-50
	

	// Constructor to instantiate a random location unless specified otherwise 
	
	public GameObject()
	{
		x = Rx;
		y = Ry;
		size = Rsize;
	}
	
	
	
// methods 
	public int getSize() {return size;}
    protected void setSize(int s) {size = s;}
    public double getX() {return x;}
    public double getY() {return y;}
    
    // general method can be overridden with an empty body for fixed objects
    public void setLocation(double nX, double nY) 
    {
    	// check if location is within the boundaries
     if ((nX >= 0 && nX <= 1024) && (nY >= 0 && nY <= 768)) {
     x=nX;
     y=nY;
     }
    }
    
    public int getColor() {return color;}
    
    public void setColor(int r, int g, int b) {
     color = ColorUtil.rgb(r, g, b);
    }
    
    public String toString() {
    	 String desc =  "Loc=" + Math.round(x) + " ," + Math.round(y) + " Size=" + size + " color: " + "[" + ColorUtil.red(color) + ","
    	+ ColorUtil.green(color) + ","
    	+ ColorUtil.blue(color) + "]";
    	 return desc ;
    	} 
}
    
