package com.mycompany.a1;

public class FoodStation extends Fixed{

	private int capacity;

	  
	public FoodStation() {
		//call constructor of fixed which will assign a random location and a random size
		super();
		setColor(0,128,0); // green
		//initial capacity is set proportional to its size
		capacity = (int) (0.5 * getSize());
	}
	
	  
	
	public int getCapacity() {
		return capacity;
	}
	
	// a method to decrease capacity of the food station by the amount of food consumed. 
	public void decCapacity() {
		capacity = 0;
	}
	
	
	
	@Override
	public String toString() {
		 String parentDesc = super.toString();
		 String myDesc = " Capacity=" + capacity;
		 return "FoodStation:" + parentDesc + myDesc ;
		} 
	  
}
