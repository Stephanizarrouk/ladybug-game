package com.mycompany.a1;
import java.util.*;

import com.codename1.charts.util.ColorUtil;

public class GameWorld {

	private int clockTime = 0;
	private int lives = 3;
	private boolean exitRequested = false;
	private Vector v = new Vector(9,3);
	
	public void init() {
	
		v.addElement(new Flag(100,100, 1));
		v.addElement(new Flag(300,100, 2));
		v.addElement(new Flag(700,250, 3));
		v.addElement(new Flag(500,500, 4));
		v.addElement(new Flag(120,600, 5));
		v.addElement(new Ladybug(100,100)); 
		v.addElement(new Spider());
		v.addElement(new Spider());
		v.addElement(new FoodStation());
		v.addElement(new FoodStation());
		v.addElement(new FoodStation());
		
	}

	
	public void accelerate() {
		for (int i=0; i< v.size();i++) {
		if(v.elementAt(i) instanceof Ladybug) {
			Ladybug myLadybug = (Ladybug) v.elementAt(i);
			myLadybug.updateSpeed(1); // increase speed by 1
			System.out.println("The Speed of LadyBug has increased by 1. Current Speed is: " + myLadybug.getSpeed());
		}
	 } // for loop
	}// accelerate
	
	public void map() {
		for (int i=0; i< v.size();i++) {
			 System.out.println(v.elementAt(i).toString()); // process each element in the vector array to string method
		   }// end for loop
	} // end of map()
	
	// this function decreases the speed of the ladybug by 1 
	//this function can be changed to have a for loop instead of manually accessing the Ladybug in the vector array 
   public void 	brake() {
	   for (int i=0; i< v.size();i++) {
	      if(v.elementAt(i) instanceof Ladybug) {
			Ladybug myLadybug = (Ladybug) v.elementAt(i);
			myLadybug.updateSpeed(-1); // decrease speed by 1
			System.out.println("Brake applied. current speed of the LadyBug is:  " + myLadybug.getSpeed());
			
		}
	   }// for loop
   }// brake
	   
	// this function changes the heading of the ladybug by 5 degrees to the left 
	public void headLeft() {
		for (int i=0; i< v.size();i++) {
		 if(v.elementAt(i) instanceof Ladybug) {
				Ladybug myLadybug = (Ladybug) v.elementAt(i);
				myLadybug.updateHeading(-5); // change heading to the left by 5
				System.out.println("changed heading to the left by 5 degrees. Current heading=   " + myLadybug.getHeading());
			} //if stmnt  
		}// for Loop
   }//headLeft

	// this function changes the heading of the ladybug by 5 degrees to the left 
	public void headRight() {
		for (int i=0; i< v.size();i++) {
		 if(v.elementAt(i) instanceof Ladybug) {
				Ladybug myLadybug = (Ladybug) v.elementAt(i);
				myLadybug.updateHeading(5); // change heading to the right by 5
				System.out.println("changed heading to the right by 5 degrees. Current heading=   " + myLadybug.getHeading());
			}   
		  }// for loop
		 }// headRight
	
	public void flagReached(int x) {
		for(int i=0; i< v.size(); i++)
		{
			// loop through to find the ladybug
			if(v.elementAt(i) instanceof Ladybug)
			{
			 Ladybug myLadybug = (Ladybug) v.elementAt(i);	
			  if(myLadybug.getLastflagR() + 1 == x) {
				  myLadybug.setLastFlagR(x);
				  System.out.println("New Flag reached nunber# " + myLadybug.getLastflagR());
			  }
			}
		  }
	   }
	
	public void foodRefill() {
		int temp = 0; // to hold the capacity of the foodstation
		Ladybug myLadybug;
		FoodStation fs;
		
		for(int i=0; i< v.size(); i++)
		{
			// check to find the first foodstation
			if(v.elementAt(i) instanceof FoodStation) {
				// assign it to the locale variable
				fs = (FoodStation) v.elementAt(i);
				// check if FoodStation is not empty
				if(fs.getCapacity()!=0) 
				{
				  temp = fs.getCapacity();
				  fs.decCapacity(); // decrease the capacity of the food station
				  fs.setColor(0, 125, 0); // fade the color of the FoodStation to light green
				  v.addElement(new FoodStation());
				  break; 
				}
			}
			
		}// end of for loop.
		for(int j=0; j< v.size(); j++) {
			// loop through to find the ladybug
			if(v.elementAt(j) instanceof Ladybug)
			{
			 myLadybug = (Ladybug) v.elementAt(j);
			 myLadybug.updatefoodLevel(temp);// update the foodlevel of the Ladybug
			 System.out.println("refilled food. current food Level is: " + myLadybug.getFoodLevel());
			}
		}    
    }// end of FoodRefill
	
	// pretend a spider - LadyBug collision. This method finds the Ladybug and decrease its health and fades its color
	public void spiderCollision() {
		for(int j=0; j< v.size(); j++) {	
			if(v.elementAt(j) instanceof Ladybug) {
				Ladybug myLadybug = (Ladybug) v.elementAt(j);
				myLadybug.dechealthLevel();
				int newRed = ColorUtil.red(myLadybug.getColor()) - 50; // fades the color red
				myLadybug.setColor(newRed, ColorUtil.green(myLadybug.getColor()), ColorUtil.blue(myLadybug.getColor()));
			}
		}
	}
	
	// this method assumes that the game clock has ticked. it checkes each object in the vector array and does the following:
	//changes the heading of the spider( 5) to the right
	// moves the movable objects
	// reduce the food level of the ladybug
	// increase time by one
	
	public void tick() {
		for(int j=0; j< v.size(); j++) {	
			if(v.elementAt(j) instanceof Spider) {
				Spider sp = (Spider) v.elementAt(j);
				sp.randomHeading();
			}
			
			if(v.elementAt(j) instanceof Movable) {
				Movable mobj = (Movable) v.elementAt(j);
				mobj.move();
			}
			
			if(v.elementAt(j) instanceof Ladybug) {
				Ladybug myLadybug = (Ladybug) v.elementAt(j);
				myLadybug.reducefoodLevel();;
			}
		}
		clockTime ++;
	}
	
	
	//displays the current game status
	public void displayStatus() {
		System.out.println("Number of Lives Left:" + lives);
		System.out.println("Time elapsed: " + clockTime);
		for(int j=0; j< v.size(); j++) {
			if(v.elementAt(j) instanceof Ladybug) {
			 Ladybug myLadybug = (Ladybug) v.elementAt(j);
			 System.out.println("Last Flag reached: " + myLadybug.getLastflagR());
			 System.out.println("Food Level: " + myLadybug.getFoodLevel());
			 System.out.println("Health Level: " + myLadybug.gethealthLevel());
			}
		}
		
	}
	
	//asks user to confirm exit
	public void exit() {
		exitRequested = true;
		System.out.println("Are you sure you want to exit this awesome game? enter 'y' for yes :( or'n' for No :)");
	}
	
	public void exitY() {
		if(exitRequested)
		{System.out.println("Exiting now...bye!");
		 System.exit(0);
		} 
		else
		 {System.out.println("Exit has not been requested yet. enter 'x' first to exit");}
	}
	
	public void exitN() {
		if(exitRequested)
		{System.out.println("you have canceled the exit request");}
		else
		 {System.out.println("Exit has not been requested yet. enter 'x' first to exit");}
		
		exitRequested = false;
	}
	
}
	
