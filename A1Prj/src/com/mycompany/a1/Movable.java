package com.mycompany.a1;



public abstract class Movable extends GameObject {

	private int speed;
	private int heading;
	
	

	public int getSpeed()
	{
		return speed;
	}
	
	public int getHeading() {
		return heading;
	}
	
	public void setSpeed (int s) {
		speed= s;
	}
	
	public void setHeading (int h) {
		heading= h;
	}
	
	//a move method to update location based on heading and speed
	public void move() {
		int deltaX = (int) (speed*Math.cos(Math.toRadians(90-heading)));
		int deltaY = (int) (speed*Math.sin(Math.toRadians(90-heading)));;
		setLocation(getX()+deltaX, getY() + deltaY);
	}

}
