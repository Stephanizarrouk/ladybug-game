package com.mycompany.a1;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import java.lang.String;

public class Game extends Form{
	private GameWorld gw;
	public Game() {
		gw = new GameWorld();
		gw.init();
		play();
	}
	
	private void play() {
		Label myLabel = new Label ("Enter a Command:");
		this.addComponent(myLabel);
		final TextField myTextField= new TextField();
		this.addComponent(myTextField);
		this.show();
		
		myTextField.addActionListener(new ActionListener() {
			
			 public void actionPerformed (ActionEvent evt) {
			
				 String sCommand = myTextField.getText().toString();
				 myTextField.clear();
				 switch (sCommand.charAt(0)) {
				 	case 'm':
				 	  gw.map();
				 	  break;
				 	case 'a':
				 	  gw.accelerate();
				 	  break;
				 	case 'f':
				      gw.foodRefill();
				      break;
				 	case 'b':
					  gw.brake();
					  break;
				 	case 'g':
					  gw.spiderCollision();
					  break;
				 	case 'l':
					  gw.headLeft();
					  break;  
				 	case 'r':
					  gw.headRight();
					  break;
				 	case 't':
					  gw.tick();
					  break;
				 	case 'd':
					  gw.displayStatus();;
					  break;
					case 'x':
					  gw.exit();
					  break;
					case 'y':
					  gw.exitY();
					  break;
					case 'n':
					  gw.exitN();
					  break;
				 	case '1' :
				 	  gw.flagReached(1);
				 	 break;
				 	case '2' :
				 	  gw.flagReached(2);
				 	 break;
				 	case '3' :
				 	  gw.flagReached(3);
				 	 break;
				 	case '4' :
				 	  gw.flagReached(4);
				 	 break;
				 	case '5' :
				 	  gw.flagReached(5);
				 	 break;
				 	case '6' :
				 	  gw.flagReached(6);
				 	 break;
				 	case '7' :
				 	  gw.flagReached(7);
				 	 break;
				 	case '8' :
				 	  gw.flagReached(8);
				 	 break;
				 	case '9' :
				 	  gw.flagReached(9);
				 	 break;
				 }//switch
	    }// actionPerformed
		}// new ActionListener()
		); // addActionListner
	    } //play
	

}
