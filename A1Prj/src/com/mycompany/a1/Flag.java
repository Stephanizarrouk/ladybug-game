package com.mycompany.a1;

public class Flag extends Fixed{

	private int sequenceNumber;
	
	public Flag(double x, double y, int sq) {
		super(100,33,55,7,x,y); // call super constructor to set color ( first three parms, size, and location)
		sequenceNumber = sq;
	}
	
	public int getSqnumber() {return sequenceNumber;}
	
	
	// flag cannot change color once created
	@Override
	public void setColor(int r, int g, int b) {
		
	}
	
	 
	  
	@Override
	public String toString() {
		 String parentDesc = super.toString();
		 String myDesc = " SequenceNumber =" + sequenceNumber;
		 return "Flag:" + parentDesc + myDesc ;
		} 
	  
	  
}
