package com.mycompany.a1;



public class Spider extends Movable{
	
	
	public Spider() {
        super(); // sets random location and size
        super.setColor(255,255,255); //black
		super.setSpeed(rand.nextInt(10) + 5); // random speed from 5 - 15
		super.setHeading(rand.nextInt(359)); // random int from 0 - 359
	}
	
	// cannot change its color
	@Override
	public void setColor(int r, int g, int b) {} 
	
	// prevents others and this class from setting a specific heading
	@Override
	public void setHeading(int h) {}
	
	// spider cannot change its speed and does not allow others to do so
	@Override
	public void setSpeed(int s) {}
	
	
	// random heading. change the heading of the spider by adding a 5 degree 
	public void randomHeading() {  
     super.setHeading(getHeading() + 5);
	}
    
	@Override
	public String toString() {
		 String parentDesc = super.toString();
		 String myDesc = " Speed=" + getSpeed() + " heading=" + getHeading() ;
		 return "Spider: "+ parentDesc + myDesc ;
		}
	
	
	
}
