package com.mycompany.a1;

public interface ISteerable {

	public void updateHeading(int h);
	public void updateSpeed(int s);
}
