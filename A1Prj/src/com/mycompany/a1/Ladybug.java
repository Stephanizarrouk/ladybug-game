package com.mycompany.a1;

public class Ladybug extends Movable implements ISteerable{

	private int initialmaxSpeed;
	private int foodLevel;
	private int foodConsumptionRate;
	private int healthLevel;
	private int lastFlagReached;
	private int maximumSpeed;
	
	public Ladybug(double x, double y) {
		setLocation(x, y);// the location of the first flag
		initialmaxSpeed = 15;
		maximumSpeed = initialmaxSpeed;
		setHeading(10);
		super.setSpeed(5); // initial speed 
		setColor(255,0,0); // red
		lastFlagReached =1;
		healthLevel = 10;
		foodConsumptionRate = 3;
		foodLevel = 10;
		
	}
	
	
	
	// override speed to check for the maximum allowed speed
	@Override
	public void setSpeed(int s)
	{
		if (s > maximumSpeed)
			{super.setSpeed(maximumSpeed);}
		else
			super.setSpeed(s);
		}	

    public void reducefoodLevel() {
    	foodLevel -= foodConsumptionRate;
    	if (foodLevel <= 0) // no food, speed in zero
    		setSpeed(0);
    }

    public void updatefoodLevel( int f) {
    	foodLevel += f;  // update foodLevel with the amount of f. 'capacity' of the food station
    }
    
    public int getLastflagR() {
    	return lastFlagReached;
    }
    
    public void setLastFlagR( int fr) {
    	lastFlagReached= fr;
    }
    
    public void dechealthLevel() {
    	healthLevel-=2;
    	updateMaxSpeed((int)(healthLevel * 0.1 * initialmaxSpeed));
    }
    
    private void updateMaxSpeed(int maxS)
    {
    	maximumSpeed = maxS;
    	// check if current speed is within the range of maximumSpeed 
    	if (getSpeed() > maximumSpeed )
    		setSpeed(maximumSpeed);	
    }
    
    public int getMaxSpeed() {
    	return maximumSpeed; 
    }

    
    public int gethealthLevel() {
    	return healthLevel;
    }
    
    public void updateHeading(int h) {
    	setHeading(getHeading() + h);
    }
   
    public void updateSpeed(int s) {
    	setSpeed(getSpeed()+ s);
    }
    
    public int getFoodLevel() {
		return foodLevel;
	}
    
    @Override
	public String toString() {
		 String parentDesc = super.toString();
		 String myDesc = " Speed= " + getSpeed() + " Heading= "+ getHeading() + " MaxSpeed= " + getMaxSpeed() + " FoodConsumptionRate= "+ foodConsumptionRate;
		 return "LadyBug:  "+parentDesc + myDesc ;
		} 
	  
}
